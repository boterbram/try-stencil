import {
  Component,
  h,
  Host,
  Prop,
  State,
  Event,
  EventEmitter,
} from '@stencil/core';

@Component({
  tag: 'myneva-alert',
  styleUrl: 'alert.scss',
  shadow: true,
})
export class Alert {
  /**
   * The first name
   */
  @Prop() dismissable: boolean;
  @Prop() accent: string = 'info';

  @State() shown = true;

  @Event() close: EventEmitter

  onClose() {
    this.shown = false;
    this.close.emit();
  }

  render() {
     return this.shown ? <Host
      class={
        {
          'alert': true,
          [`alert-${this.accent}`]: true,
        }
      }
    >
      {
        this.dismissable
          ? <button onClick={() => this.onClose()}>close</button>
          : ''
      }
      <h4>
        {this.shown}
        <slot name="title"/>
      </h4>
      <slot/>
    </Host> : '';
  }
}
